This project is based on the Article by A. Martín del Rey et al.: "A Model Based
on Cellular Automata to Simulate Epidemic Diseases"

It shows how an epidemic disease would spread through a determined area by using
different parameters extensively explained on the previously mentioned article.

In this small visualization, the cells can have 3 different colours:
- Blue cells are zones where most people are susceptible to becoming ill.
- White cells are zones where most people are infected.
- Red cells are zones where most people are recovered. This can be caused by
overcoming the illness, but also by being vaccinated.

This project runs on Python 3.