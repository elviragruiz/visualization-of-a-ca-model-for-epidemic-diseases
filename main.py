from __future__ import division
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
from pylab import *

# NxN space
N = 50

# Starting parameters
recovery_rate = 0.4
virulence = 0.6
vac_rate = 0.01

# En esta implementación todas las células tienen 
# el mismo factor de conexión y de movimiento
connection_factor = 1
movement_factor = 0.5
mu = virulence * connection_factor * movement_factor
real =[[(1,0,0) for b in range(N)] for a in range(N)] 
counter = 0

# Como en este ejemplo todas las células tienen la misma población, no usaremos
# las variables N

#cell_population = 100

def main():

    #Focos iniciales    
    real[N-1][N-1]=(0,1,0)
    real[10][10]=(0,1,0)

    values = get_values(real)
    
    def ontype(event):
        ''' function that is called on key event (press '1')'''
        global real,counter
        if event.key == '1':
            counter += 1
            real = next_state(real)
            values = get_values(real)
            fig.gca().clear()
            pcolor(values,edgecolors='k',cmap=cm.RdBu,linewidth=1) 
            plt.title(("CA simulation of a disease. T = "+str(counter)))
            fig.canvas.draw()
    fig = figure()
    fig.canvas.mpl_connect('key_press_event',ontype)
    plt.pcolor(values, edgecolors='k',cmap=cm.RdBu,linewidth=1)
    plt.title("CA simulation of a disease. T = 0 (press 1)")
    plt.show() 


#n = length of an array, cell_pos = position for which we want to get neighbors
def get_values(real):
    res = []
    for row in real:
        new_row = []
        for cell in row:
            if cell[0] >= cell[1] and cell[0] >= cell[2]:
                # 3 es rojo e indica SUSCEPTIBLE
                    new_row.append(3)
            elif cell[1] > cell[0] and cell[1] >= cell[2]:
                # 2 es blanco e indica INFECTADO
                new_row.append(2)
            else:
                # 1 es azul e indica RECUPERADO
                new_row.append(1)
        res.append(new_row)
    return res

def next_state(l):
    new_state = []
    a = 0
    for row in l:
        new_row=[]
        b = 0
        for cell in row:
            new_cell = new_state_cell(l,[a,b])
            b = b + 1
            new_row.append(new_cell)
        a = a + 1
        new_state.append(new_row)
    return new_state

#pos = [x,y]
def new_state_cell(l,pos):
    nbrs = get_neighbors(len(l),pos)
    i = get_new_i(l, pos, nbrs)
    s = get_new_s(l, pos, nbrs)
    r = get_new_r(l, pos)
    return (s,i,r)

def get_new_i(l,pos,nbrs):
    old_s = l[pos[0]][pos[1]][0]
    old_i = l[pos[0]][pos[1]][1] 
    nbrs_value = 0
    for neig in nbrs:
        nbrs_value += l[neig[0]][neig[1]][1] * mu
    nbrs_infects = nbrs_value * old_s
    new_i = (1 - recovery_rate) * old_i + old_s * virulence * old_i + nbrs_infects 
    if new_i > 1:
        new_i = 1
    if new_i < 0:
        new_i = 0 
    return round(new_i,2)

def get_new_s(l,pos,nbrs):
    old_s = l[pos[0]][pos[1]][0]
    old_i = l[pos[0]][pos[1]][1] 
    nbrs_value = 0
    for neig in nbrs:
        nbrs_value += l[neig[0]][neig[1]][1] * mu
    nbrs_infects = nbrs_value * old_s
    new_s = (1 - vac_rate) * old_s - (old_s * virulence * old_i) - nbrs_infects
    if new_s > 1:
        new_s = 1
    if new_s < 0:
        new_s = 0
    return round(new_s,2)

def get_new_r(l,pos):
    old_r = l[pos[0]][pos[1]][2]
    old_i = l[pos[0]][pos[1]][1]
    old_s = l[pos[0]][pos[1]][0]
    new_r= old_r + recovery_rate * old_i + vac_rate * old_s
    if new_r > 1:
        new_r = 1
    if new_r < 0:
        new_r = 0
    return round(new_r,2)

def get_neighbors(n,cell_pos):
    res = []
    if cell_pos[0]-1 >= 0:
        for i in range(cell_pos[1]-1,cell_pos[1] + 2):
            if cell_pos[0]-1 >= 0 and i < n and i >= 0:
                res.append([cell_pos[0]-1,i])
    if cell_pos[1]-1 >= 0:        
        res.append([cell_pos[0],cell_pos[1]-1])
    if cell_pos[1] + 1 < n:
        res.append([cell_pos[0],cell_pos[1]+1])
    for i in range(cell_pos[1]-1,cell_pos[1] + 2):    
        if i < n and cell_pos[0]+1 < n and i >= 0:
            res.append([cell_pos[0]+1,i]) 
    return res

if __name__ == "__main__":
    main()
